/*
 Navicat Premium Data Transfer

 Source Server         : SENAI
 Source Server Type    : MariaDB
 Source Server Version : 100325
 Source Host           : localhost:3306
 Source Schema         : senai

 Target Server Type    : MariaDB
 Target Server Version : 100325
 File Encoding         : 65001

 Date: 06/12/2020 20:09:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for atividades
-- ----------------------------
DROP TABLE IF EXISTS `atividades`;
CREATE TABLE `atividades`  (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `max_carga_horaria` tinyint(3) UNSIGNED NOT NULL,
  `documentacao` tinyint(1) NOT NULL,
  `observacao` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `modalidades_id` tinyint(4) NOT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp,
  `updated_at` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_atividades_modalidades`(`modalidades_id`) USING BTREE,
  CONSTRAINT `fk_atividades_modalidades` FOREIGN KEY (`modalidades_id`) REFERENCES `modalidades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of atividades
-- ----------------------------

-- ----------------------------
-- Table structure for modalidades
-- ----------------------------
DROP TABLE IF EXISTS `modalidades`;
CREATE TABLE `modalidades`  (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp,
  `updated_at` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of modalidades
-- ----------------------------

-- ----------------------------
-- Table structure for registros_atividade
-- ----------------------------
DROP TABLE IF EXISTS `registros_atividade`;
CREATE TABLE `registros_atividade`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carga_horaria` tinyint(4) NOT NULL,
  `documentacao` tinyint(1) NOT NULL,
  `url_documentacao` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `observacao` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `usuarios_id` mediumint(9) NOT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp,
  `updated_at` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `status`) USING BTREE,
  INDEX `fk_registros_atividade_status1`(`status`) USING BTREE,
  INDEX `fk_registros_atividade_usuarios1`(`usuarios_id`) USING BTREE,
  CONSTRAINT `fk_registros_atividade_status1` FOREIGN KEY (`status`) REFERENCES `status` (`id_status`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registros_atividade_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of registros_atividade
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id_roles` tinyint(4) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp,
  `updated_at` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_roles`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'Administrador', '2020-11-27 17:39:12', '0000-00-00 00:00:00');
INSERT INTO `roles` VALUES (2, 'Secretaria', '2020-12-06 20:07:24', '0000-00-00 00:00:00');
INSERT INTO `roles` VALUES (3, 'Coordenador', '2020-12-06 20:07:39', '0000-00-00 00:00:00');
INSERT INTO `roles` VALUES (4, 'Aux. Secretaria', '2020-12-06 20:07:52', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for status
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status`  (
  `id_status` tinyint(4) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp,
  `updated_at` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of status
-- ----------------------------

-- ----------------------------
-- Table structure for turmas
-- ----------------------------
DROP TABLE IF EXISTS `turmas`;
CREATE TABLE `turmas`  (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `nome_turma` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ano` tinyint(4) NOT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp,
  `updated_at` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of turmas
-- ----------------------------

-- ----------------------------
-- Table structure for turmas_has_usuarios
-- ----------------------------
DROP TABLE IF EXISTS `turmas_has_usuarios`;
CREATE TABLE `turmas_has_usuarios`  (
  `turmas_id` smallint(6) NOT NULL,
  `usuarios_id` mediumint(9) NOT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp,
  `updated_at` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`turmas_id`, `usuarios_id`) USING BTREE,
  INDEX `fk_turmas_has_usuarios_usuarios1`(`usuarios_id`) USING BTREE,
  CONSTRAINT `fk_turmas_has_usuarios_turmas1` FOREIGN KEY (`turmas_id`) REFERENCES `turmas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_turmas_has_usuarios_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of turmas_has_usuarios
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `documento` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `data_nasc` date NOT NULL,
  `email` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `roles_id` tinyint(4) NOT NULL,
  `URL_perfil` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL DEFAULT current_timestamp,
  `updated_at` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_alunos_roles1`(`roles_id`) USING BTREE,
  CONSTRAINT `fk_alunos_roles1` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id_roles`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
