import { NestApplicationOptions, ValidationPipe } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import rateLimit from 'express-rate-limit'
import helmet from 'helmet'
import { AppModule } from './app.module'

async function bootstrap() {
  const options: NestApplicationOptions =
    process.env.DEBUG_LOGGING === 'true' ? {} : { logger: ['warn', 'error'] }

  const app = await NestFactory.create(AppModule, options)

  app.useGlobalPipes(new ValidationPipe())

  app.use(helmet())
  app.enableCors({
    origin: true,
    credentials: true,
  })
  app.use(
    rateLimit({
      windowMs: 60000,
      max: 100,
    }),
  )

  await app.listen(3000)
}
bootstrap()
