import { EntityRepository, QueryOrder } from '@mikro-orm/core'

export async function pagination(
  queryString: string[],
  repository: EntityRepository<any>,
  populate: string[] = [],
) {
  const options = {}
  const pagination = {}

  const orderBy = queryString['orderBy']
  const desc = queryString['isDesc'] === 'true'
  const page = Number(queryString['page']) || 1
  const limit = Number(queryString['limit']) || 50
  const props = queryString['props'] ? JSON.parse(queryString['props']) : {}

  if (orderBy) {
    pagination['orderBy'] = orderBy
    pagination['isDesc'] = desc

    const order = {}
    order[orderBy] = desc ? QueryOrder.DESC : QueryOrder.ASC

    options['orderBy'] = order
  }

  pagination['page'] = page

  options['offset'] = (page - 1) * limit
  options['limit'] = pagination['rowsPerPage'] = limit >= 100 ? 100 : limit

  options['populate'] = populate

  const [rows, count] = await repository.findAndCount(props, options)

  pagination['rowsNumber'] = count

  return { pagination, rows }
}
