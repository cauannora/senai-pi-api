import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  // eslint-disable-next-line prettier/prettier
  ValidatorConstraintInterface,
} from 'class-validator'
import { cpf } from 'cpf-cnpj-validator'

@ValidatorConstraint({ name: 'cpf', async: true })
class CpfConstraint implements ValidatorConstraintInterface {
  async validate(value: any) {
    if (cpf.isValid(value)) return true

    return false
  }
}

export function Cpf(validationOptions?: ValidationOptions) {
  validationOptions = {
    ...{ message: '$property is not valid CPF document' },
    ...validationOptions,
  }
  return function (object: Record<string, any>, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: CpfConstraint,
    })
  }
}
