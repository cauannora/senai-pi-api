import { MikroOrmModule } from '@mikro-orm/nestjs'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { ScheduleModule } from '@nestjs/schedule'
import { ActivityModule } from './components/activity/activity.module'
import { Activity } from './components/activity/entities/activity.entity'
import { AuthModule } from './components/auth/auth.module'
import { ClassModule } from './components/class/class.module'
import { Class } from './components/class/entities/class.entity'
import { Modality } from './components/modality/entities/modality.entity'
import { ModalityModule } from './components/modality/modality.module'
import { RecordActivity } from './components/record-activity/entities/record-activity.entity'
import { RecordActivitiesModule } from './components/record-activity/record-activity.module'
import { Role } from './components/role/entities/role.entity'
import { RoleModule } from './components/role/role.module'
import { Status } from './components/status/entities/status.entity'
import { StatusModule } from './components/status/status.module'
import { User } from './components/user/entities/user.entity'
import { UserModule } from './components/user/user.module'
@Module({
  imports: [
    ConfigModule.forRoot(),
    ScheduleModule.forRoot(),
    MikroOrmModule.forRoot({
      entities: [User, Role, Class, RecordActivity, Modality, Activity, Status],
      type: 'mariadb',
      host: process.env.MYSQL_HOST,
      port: +process.env.MYSQL_PORT,
      user: process.env.MYSQL_USERNAME,
      password: process.env.MYSQL_PASSWORD,
      dbName: process.env.MYSQL_DATABASE,
    }),
    UserModule,
    RoleModule,
    StatusModule,
    ClassModule,
    ModalityModule,
    ActivityModule,
    RecordActivitiesModule,
    AuthModule,
  ],
})
export class AppModule {}
