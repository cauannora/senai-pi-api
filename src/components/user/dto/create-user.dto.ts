import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  MaxLength,
  MinLength,
} from 'class-validator'
import { Cpf } from 'src/helpers/documento.validator'

export class CreateUserDto {
  @IsNotEmpty()
  @MaxLength(50)
  nome: string

  @IsNotEmpty()
  @Cpf()
  documento: string

  @IsNotEmpty()
  data_nasc: Date

  @IsNotEmpty()
  @IsEmail()
  email: string

  @IsNotEmpty()
  roles_id: number

  @IsNotEmpty()
  @MinLength(8)
  password: string

  @IsOptional()
  URL_perfil: string
}
