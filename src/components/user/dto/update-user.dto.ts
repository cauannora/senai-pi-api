import { PartialType } from '@nestjs/mapped-types'
import { IsEmail, IsOptional, MaxLength, MinLength } from 'class-validator'
import { Cpf } from 'src/helpers/documento.validator'
import { CreateUserDto } from './create-user.dto'

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @IsOptional()
  @MaxLength(50)
  nome: string

  @IsOptional()
  @Cpf()
  documento: string

  @IsOptional()
  data_nasc: Date

  @IsOptional()
  turmaId: number

  @IsOptional()
  @IsEmail()
  email: string

  @IsOptional()
  @MinLength(8)
  password: string

  @IsOptional()
  URL_perfil: string
}
