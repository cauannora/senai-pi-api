import { Entity, ManyToOne, PrimaryKey, Property } from '@mikro-orm/core'
import { Role } from './../../role/entities/role.entity'

@Entity()
export class User {
  @PrimaryKey({ type: 'MEDIUMINT' })
  id!: number

  @Property()
  nome: string

  @Property()
  data_nasc: Date

  @Property()
  documento: string

  @Property({ unique: true })
  email: string

  @Property({ hidden: true })
  password: string

  @Property()
  URL_perfil: string

  @Property({ type: 'SMALLINT' })
  turmaId: number

  @ManyToOne({ entity: () => Role, eager: true, fieldName: 'roles_id' })
  role: Role

  @Property()
  createdAt = Date

  @Property()
  updatedAt = Date
}
