import { EntityRepository, wrap } from '@mikro-orm/core'
import { InjectRepository } from '@mikro-orm/nestjs'
import {
  BadRequestException,
  Injectable,
  // eslint-disable-next-line prettier/prettier
  NotFoundException,
} from '@nestjs/common'
import bcrypt from 'bcrypt'
import { pagination } from 'src/helpers/pagination.helper'
import { CreateUserDto } from './dto/create-user.dto'
import { UpdateUserDto } from './dto/update-user.dto'
import { User } from './entities/user.entity'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: EntityRepository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    createUserDto.password = bcrypt.hashSync(createUserDto.password, 11)
    const result = await this.userRepository.nativeInsert(createUserDto)

    if (typeof result === 'number') return this.findOne(result)

    throw new BadRequestException()
  }

  async findAll(queryString: string[]) {
    return await pagination(queryString, this.userRepository)
  }

  async findOne(id: number) {
    const user = await this.userRepository.findOne(id)

    if (user instanceof User) return user

    throw new NotFoundException()
  }

  async check(queryString: string[]) {
    const options = {}
    const email = queryString['email']
    const document = queryString['documento']

    if (email) options['email'] = email
    if (document) options['documento'] = document

    if (Object.keys(options).length === 0) return false

    const user = await this.userRepository.findOne(options)

    if (user instanceof User)
      if (user.id === Number(queryString['id'])) return false
      else return true

    return false
  }

  async validateUser(email: string) {
    return await this.userRepository.findOne({ email })
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    if (updateUserDto.password)
      updateUserDto.password = bcrypt.hashSync(updateUserDto.password, 11)

    const user = await this.findOne(id)

    wrap(user).assign(updateUserDto)
    await this.userRepository.persist(user)

    return this.findOne(id)
  }

  async remove(id: number) {
    const result = await this.userRepository.nativeDelete(id)

    if (result === 0) throw new BadRequestException()

    return { message: 'Deleted' }
  }
}
