import { PartialType } from '@nestjs/mapped-types'
import { CreateRecordActivityDto } from './create-record-activity.dto'

export class UpdateRecordActivityDto extends PartialType(
  CreateRecordActivityDto,
) {}
