import { Entity, ManyToOne, PrimaryKey, Property } from '@mikro-orm/core'
import { User } from '../../user/entities/user.entity'

@Entity()
export class RecordActivity {
  @PrimaryKey({ type: 'INT' })
  id!: number

  @Property({ type: 'TINYINT' })
  carga_horaria: number

  @Property()
  documentacao: boolean

  @Property()
  urlDocumentacao: string

  @Property()
  observacao: string

  @Property({ type: 'TINYINT' })
  status: number

  @ManyToOne({ entity: () => User })
  usuario: User

  @Property()
  createdAt = Date

  @Property()
  updatedAt = Date
}
