import { Injectable } from '@nestjs/common'
import { CreateRecordActivityDto } from './dto/create-record-activity.dto'
import { UpdateRecordActivityDto } from './dto/update-record-activity.dto'

@Injectable()
export class RecordActivityService {
  create(createRecordActivityDto: CreateRecordActivityDto) {
    return 'This action adds a new recordActivity'
  }

  findAll() {
    return `This action returns all recordActivities`
  }

  findOne(id: number) {
    return `This action returns a #${id} recordActivity`
  }

  update(id: number, updateRecordActivityDto: UpdateRecordActivityDto) {
    return `This action updates a #${id} recordActivity`
  }

  remove(id: number) {
    return `This action removes a #${id} recordActivity`
  }
}
