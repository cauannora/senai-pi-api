import { Module } from '@nestjs/common'
import { RecordActivityController } from './record-activity.controller'
import { RecordActivityService } from './record-activity.service'

@Module({
  controllers: [RecordActivityController],
  providers: [RecordActivityService],
})
export class RecordActivitiesModule {}
