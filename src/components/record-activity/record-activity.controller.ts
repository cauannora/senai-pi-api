import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common'
import { UseGuards } from '@nestjs/common/decorators/core/use-guards.decorator'
import { AuthGuard } from '@nestjs/passport'
import { CreateRecordActivityDto } from './dto/create-record-activity.dto'
import { UpdateRecordActivityDto } from './dto/update-record-activity.dto'
import { RecordActivityService } from './record-activity.service'

@UseGuards(AuthGuard('jwt'))
@Controller('record-activity')
export class RecordActivityController {
  constructor(private readonly recordActivityService: RecordActivityService) {}

  @Post()
  create(@Body() createRecordActivityDto: CreateRecordActivityDto) {
    return this.recordActivityService.create(createRecordActivityDto)
  }

  @Get()
  findAll() {
    return this.recordActivityService.findAll()
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.recordActivityService.findOne(+id)
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateRecordActivityDto: UpdateRecordActivityDto,
  ) {
    return this.recordActivityService.update(+id, updateRecordActivityDto)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.recordActivityService.remove(+id)
  }
}
