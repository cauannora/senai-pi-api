import { Injectable } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import bcrypt from 'bcrypt'
import { User } from '../user/entities/user.entity'
import { UserService } from '../user/user.service'

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.userService.validateUser(email)

    if (user instanceof User && bcrypt.compareSync(password, user.password)) {
      return user
    }

    return null
  }

  async getToken(user: User) {
    const payload = { id: user.id, roles_id: user.role }
    return this.jwtService.sign(payload)
  }
}
