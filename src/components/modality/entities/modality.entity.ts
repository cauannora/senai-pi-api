import { Entity, PrimaryKey, Property } from '@mikro-orm/core'

@Entity()
export class Modality {
  @PrimaryKey({ type: 'TINYINT' })
  id!: number

  @Property()
  nome: string

  @Property()
  createdAt = Date

  @Property()
  updatedAt = Date
}
