import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common'
import { CreateModalityDto } from './dto/create-modality.dto'
import { UpdateModalityDto } from './dto/update-modality.dto'
import { ModalityService } from './modality.service'

@Controller('modality')
export class ModalityController {
  constructor(private readonly modalityService: ModalityService) {}

  @Post()
  create(@Body() createModalityDto: CreateModalityDto) {
    return this.modalityService.create(createModalityDto)
  }

  @Get()
  findAll() {
    return this.modalityService.findAll()
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.modalityService.findOne(+id)
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateModalityDto: UpdateModalityDto,
  ) {
    return this.modalityService.update(+id, updateModalityDto)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.modalityService.remove(+id)
  }
}
