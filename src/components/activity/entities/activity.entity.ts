import { Entity, ManyToOne, PrimaryKey, Property } from '@mikro-orm/core'
import { Modality } from '../../modality/entities/modality.entity'

@Entity()
export class Activity {
  @PrimaryKey({ type: 'SMALLINT' })
  id!: number

  @Property({ type: 'TINYINT' })
  max_carga_horaria: number

  @Property()
  documentacao: boolean

  @Property()
  observacao: string

  @ManyToOne({ entity: () => Modality })
  modadalidades_id: Modality

  @Property()
  createdAt = Date

  @Property()
  updatedAt = Date
}
