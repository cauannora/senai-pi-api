import { Entity, PrimaryKey, Property } from '@mikro-orm/core'

@Entity()
export class Class {
  @PrimaryKey({ type: 'SMALLINT' })
  id!: number

  @Property()
  nomeTurma: string

  @Property({ type: 'TINYINT' })
  ano: number

  @Property()
  createdAt = Date

  @Property()
  updatedAt = Date
}
