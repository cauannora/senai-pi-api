import { MikroOrmModule } from '@mikro-orm/nestjs'
import { Module } from '@nestjs/common'
import { Role } from '../role/entities/role.entity'
import { StatusController } from './status.controller'
import { StatusService } from './status.service'

@Module({
  imports: [MikroOrmModule.forFeature({ entities: [Role] })],
  controllers: [StatusController],
  providers: [StatusService],
})
export class StatusModule {}
