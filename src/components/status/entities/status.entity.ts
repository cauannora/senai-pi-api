import { Entity, PrimaryKey, Property } from '@mikro-orm/core'

@Entity()
export class Status {
  @PrimaryKey({ type: 'TINYINTTT' })
  id_status!: number

  @Property()
  descricao: string

  @Property()
  createdAt = Date

  @Property()
  updatedAt = Date
}
