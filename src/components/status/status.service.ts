import { EntityRepository } from '@mikro-orm/core'
import { InjectRepository } from '@mikro-orm/nestjs'
import { BadRequestException, Injectable } from '@nestjs/common'
import { Role } from '../role/entities/role.entity'
import { CreateStatusDto } from './dto/create-status.dto'
import { UpdateStatusDto } from './dto/update-status.dto'

@Injectable()
export class StatusService {
  @InjectRepository(Role)
  private readonly roleRepository: EntityRepository<Role>

  async create(createStatusDto: CreateStatusDto) {
    const result = await this.roleRepository.nativeInsert(createStatusDto)

    if (typeof result === 'number') return this.findOne(result)

    throw new BadRequestException()
  }

  findAll() {
    return `This action returns all status`
  }

  findOne(id: number) {
    return `This action returns a #${id} status`
  }

  update(id: number, updateStatusDto: UpdateStatusDto) {
    return `This action updates a #${id} status`
  }

  remove(id: number) {
    return `This action removes a #${id} status`
  }
}
