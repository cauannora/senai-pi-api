import {
  Collection,
  Entity,
  OneToMany,
  PrimaryKey,
  // eslint-disable-next-line prettier/prettier
  Property
} from '@mikro-orm/core'
import { User } from './../../user/entities/user.entity'
@Entity({ tableName: 'roles' })
export class Role {
  @PrimaryKey({ type: 'TINYINT', fieldName: 'id_roles' })
  id!: number

  @Property()
  descricao: string

  @OneToMany(() => User, (User) => User.role)
  User = new Collection<User>(this)

  @Property()
  createdAt = Date

  @Property()
  updatedAt = Date
}
