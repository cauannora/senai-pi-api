import { EntityRepository } from '@mikro-orm/core'
import { InjectRepository } from '@mikro-orm/nestjs'
import {
  BadRequestException,
  Injectable,
  // eslint-disable-next-line prettier/prettier
  NotFoundException
} from '@nestjs/common'
import { pagination } from 'src/helpers/pagination.helper'
import { CreateRoleDto } from './dto/create-role.dto'
import { UpdateRoleDto } from './dto/update-role.dto'
import { Role } from './entities/role.entity'

@Injectable()
export class RoleService {
  @InjectRepository(Role)
  private readonly roleRepository: EntityRepository<Role>

  async create(createRoleDto: CreateRoleDto) {
    const result = await this.roleRepository.nativeInsert(createRoleDto)

    if (typeof result === 'number') return await this.findOne(result)

    throw new BadRequestException()
  }

  async findAll(queryString: string[]) {
    return await pagination(queryString, this.roleRepository)
  }

  async findOne(id: number) {
    const role = await this.roleRepository.findOne(id)

    if (role instanceof Role) return role

    throw new NotFoundException()
  }

  update(id: number, updateRoleDto: UpdateRoleDto) {
    return this.roleRepository.nativeUpdate(id, updateRoleDto)
  }

  remove(id: number) {
    return this.roleRepository.nativeDelete(id)
  }
}
